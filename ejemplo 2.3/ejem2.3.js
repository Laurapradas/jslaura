'use strict'

// funciones como argumentos

// array fechas nacimiento

var years = [1995, 1993, 1989, 1985, 1997];

// funciones helper

function calcAge(age) {
    return 2019 - age;
};

function isFullAge(age) {
    return age >= 18;
}

function maxHeartRate(age) {
    if (age>=18 && age <= 81) {
        return Math.round(206.9-(0.67*age));
    } else {
        return -1;
    }
}




// main function 

function arrayCalc(arr,fn) {
    var arrRes = [];
    for (let i=0; i<arr.length;i++) {
        arrRes.push(fn(arr[i]));
    } 
    return arrRes;
}

var ages = arrayCalc(years, calcAge);
var fullAges = arrayCalc(years, isFullAge);
var heartRate = arrayCalc(ages,maxHeartRate);

console.log(ages);
console.log(fullAges);
console.log(heartRate);




