'use strict'

// funciones que devuelven funciones


function principal(job) {
    if (job === 'designer') {
        return function(name) {
            console.log(name+', can you please explain what UX design is?');
        }
    } else if (job === 'teacher') {
        return function(name) {
            console.log('what subject do you teach? '+name);
        }
    } else {
        return function(name) {
            console.log('Hello '+name+' what do you do?')
        }
    }
}

var teacherQuestion = principal('teacher');
var designerQuestion = principal('designer');
teacherQuestion('paola');
designerQuestion('laura');
// designerQuestion('designer')('stella');
console.log();