'use strict'

// ejemplo de como crear objetos por medio de constructores object.create


//para año actual

var date = new Date();
var year = date.getFullYear();

// metodo antiguo

var adan = {
    name: 'Adan', 
    yearOfBirth: 1989,
    job: 'contable',
    calcAge: function (){
        this.age = 2019 - this.yearOfBirth;
        console.log("la edad de "+ this.name + " es "+ this.age + " años");
    }
};

// object.create


var Person = function(name, yearOfBirth,job) {
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
}

var personProto = {
    calcAge: function(){
        this.age = year-this.yearOfBirth;
        console.log(this.age);
    }
};

// añadir propiedades individualmente

var laura = Object.create(personProto);
laura.name = 'Laura';

// añadir propiedades conjuntamente

var monica = Object.create(personProto,
    {
    name: {value:'Monica'},
    yearOfBirth: {value:1990},
    job:{value:'CM'},
});

console.log(monica);
monica.calcAge(); 