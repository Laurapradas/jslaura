'use strict'

// ejemplo de como crear objetos por medio de constructores

// metodo antiguo
var laura = {
    name: 'Laura',
    yearOfBirth: 1995,
    job: 'junior front end developer',
    calcAge: function(){
        this.age = 2019 - this.yearOfBirth;
        console.log("la edad de "+ this.name + " es "+ this.age + " años");
    } 
};

var adan = {
    name: 'Adan', 
    yearOfBirth: 1989,
    job: 'contable',
    calcAge: function (){
        this.age = 2019 - this.yearOfBirth;
        console.log("la edad de "+ this.name + " es "+ this.age + " años");
    }
};
console.log("Objetos creados sin constructor", laura, adan);
// metodo constructor

// var Person = function (name, yearOfBirth, job) {
//  this.name = name;
//  this.yearOfBirth = yearOfBirth;
//  this.job = job;
// }
// var laura = new Person('Laura', 1995, 'junior front end developer');
// var adan = new Person('Adan', 1989, 'contable');
console.log("Objetos creados con constructor", laura, adan);

// CONSTRUCTOR CON METODOS

var Person = function (name, yearOfBirth, job) {
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
    this.calcAge = function(){
        this.age = 2019 - this.yearOfBirth;
        console.log("la edad de "+ this.name + " es "+ this.age + " años");
    }
   }
var laura = new Person('Laura', 1995, 'junior front end developer');
laura.calcAge();
adan.calcAge();

// constructor sin metodos, metodos mediante prototype

var Person = function (name, yearOfBirth, job) {
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
   
   }

Person.prototype.calcAge = function(){
    this.age = 2019 - this.yearOfBirth;
    console.log("la edad de "+ this.name + " es "+ this.age + " años");
};

Person.prototype.lastName = 'De Dios';

var adan = new Person('Adan',1989,'contable');
console.log("Objetos creados con constructor y prototype: ", adan);

adan.calcAge();
console.log('objetos creados con constructor y prototype despues de llamar calcage',adan);

console.log(Person.prototype);